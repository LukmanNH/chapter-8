import React from "react";
import { useState } from "react";
import FormInput from "../Components/FormInput";
import { Link } from "react-router-dom";
import ButtonSubmit from "../Components/ButtonSubmit";
import PlayerCard from "../Components/PlayerCard";

const FormUpdatePlayer = () => {
  const [showPlayer, setShowPlayer] = useState(false);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [exp, setExp] = useState("");
  const [lvl, setLvl] = useState("");

  const updatePlayer = () => {
    setShowPlayer(true);
  };

  const handleNameInput = (e) => {
    setName(e.target.value);
  };

  const handleEmailInput = (e) => {
    setEmail(e.target.value);
  };

  const handleExpInput = (e) => {
    setExp(e.target.value);
  };

  const handleLvlInput = (e) => {
    setLvl(e.target.value);
  };

  return (
    <>
      <div className="container mx-auto p-10">
        <div className="mb-4 text-lg font-semibold text-[#FF8A00]">
          <Link className="hover:border-b-2 border-blue-300" to="/">
            Back to home
          </Link>
        </div>
        <div className="container border-b-pink-400 border-b-[3px] mb-8">
          <p className="text-3xl font-medium text-[#808080] pb-2">
            Update Player
          </p>
        </div>
        <div className="container grid grid-cols-2">
          <FormInput
            name="username"
            placeholder="username"
            onChange={(e) => handleNameInput(e)}
            value={name}
          />
          <FormInput
            name="email"
            placeholder="email"
            onChange={(e) => handleEmailInput(e)}
            value={email}
          />
          <FormInput
            name="exp"
            placeholder="experience"
            onChange={(e) => handleExpInput(e)}
            value={exp}
          />
          <FormInput
            name="level"
            placeholder="level"
            onChange={(e) => handleLvlInput(e)}
            value={lvl}
          />
        </div>
        <ButtonSubmit btnName={"Update"} red={false} onClick={updatePlayer} />
        <div className="mt-4">
          {showPlayer ? (
            <PlayerCard name={name} email={email} exp={exp} lvl={lvl} />
          ) : null}
        </div>
      </div>
    </>
  );
};

export default FormUpdatePlayer;
