import React from "react";
import { Link } from "react-router-dom";
import PlayerCard from "../Components/PlayerCard";
import { useState } from "react";

const Player = () => {
  const [search, setsearch] = useState("");
  const data = [
    {
      name: "luckman004",
      email: "luckman@gmail.com",
      experience: 5000,
      level: 10,
    },
    {
      name: "naisaaaaa",
      email: "naisaaaaa@gmail.com",
      experience: 300,
      level: 2,
    },
    {
      name: "ganaisssss",
      email: "ganaisssss@gmail.com",
      experience: 4500,
      level: 9,
    },
  ];

  return (
    <>
      <div className="container p-10 mx-auto">
        <div className="container p-5">
          <div className="drop-shadow-xl w-[230px] bg-white p-3">
            <div className="">
              <input
                onChange={(e) => setsearch(e.target.value)}
                type="text"
                name="search"
                placeholder="Search What You Want"
                className="p-3 border-b-2 mr-1"
              />
            </div>
          </div>
        </div>
        <div className="container p-5">
          <div className="grid grid-cols-4 gap-4">
            {data
              .filter((item) => {
                if (search === "") {
                  return item;
                } else if (
                  item.name.toLowerCase().includes(search.toLowerCase()) ||
                  item.email.toLowerCase().includes(search.toLowerCase()) ||
                  item.experience.toString().includes(search) ||
                  item.level.toString().includes(search)
                ) {
                  return item;
                }
              })
              .map((item, idx) => (
                <PlayerCard
                  key={idx}
                  name={item.name}
                  email={item.email}
                  exp={item.experience}
                  lvl={item.level}
                />
              ))}
          </div>
        </div>
      </div>
      <Link to={"/FormAddPlayer"}>
        <div className="rounded-full bg-blue-400 absolute bottom-10 right-10 p-5 text-white text-lg font-semibold hover:drop-shadow-2xl transition hover:duration-300 cursor-pointer">
          Tambah
        </div>
      </Link>
    </>
  );
};

export default Player;
