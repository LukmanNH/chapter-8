import React from "react";
import ButtonSubmit from "../Components/ButtonSubmit";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router-dom";

const DetailPlayer = () => {
  const router = useNavigate();

  const goToUpdateFormPage = () => {
    let path = "/FormUpdatePlayer";
    router(path);
  };
  return (
    <div className="container p-10 flex flex-col mx-auto">
      <div className="mb-4 text-lg font-semibold text-[#FF8A00]">
        <Link className="hover:border-b-2 border-blue-300" to="/">
          Back to home
        </Link>
      </div>
      <div className="container bg-[#EEF2F5] drop-shadow-sm w-96 mb-5">
        <div className="p-5">
          <p className="text-lg font-semibold">luckman004</p>
          <p>luckmanhakim004@gmail.com</p>
          <span className="text-green-800">Exp: 5000</span>
          <div className="w-[5px] h-[5px] bg-[#808080] mx-1 rounded-full inline-block -translate-y-1"></div>
          <span>Lv. 10</span>
        </div>
      </div>
      <div className="flex space-x-4">
        <ButtonSubmit
          btnName={"Update"}
          red={false}
          onClick={goToUpdateFormPage}
        />
        <ButtonSubmit btnName={"Delete"} red={true} />
      </div>
    </div>
  );
};

export default DetailPlayer;
