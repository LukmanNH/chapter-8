import React from "react";

const FormInput = ({ name, placeholder, onChange, value }) => {
  return (
    <div className="mb-5">
      <p className="text-2xl text-[#808080] mb-3">{name}</p>
      <input
        type="text"
        name={name}
        value={value}
        onChange={onChange}
        className="border rounded-lg w-60 py-1 px-3"
        placeholder={`Enter your ${placeholder}`}
      />
    </div>
  );
};

export default FormInput;
