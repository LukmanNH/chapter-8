import React from "react";

const ButtonSubmit = ({ btnName, red, onClick }) => {
  return (
    <button
      onClick={() => onClick()}
      type="submit"
      className={`rounded-lg py-1 px-3 ${
        !red ? "bg-blue-400" : "bg-red-500"
      } text-white text-lg font-semibold hover:drop-shadow-2xl transition hover:duration-300 cursor-pointer`}
    >
      {btnName}
    </button>
  );
};

export default ButtonSubmit;
