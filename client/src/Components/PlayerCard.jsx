import React from "react";
import { Link } from "react-router-dom";

const PlayerCard = ({ name, email, exp, lvl }) => {
  return (
    <Link to="/detail-player">
      <div className="container bg-[#EEF2F5] drop-shadow-sm hover:drop-shadow-xl hover:duration-300 cursor-pointer transition">
        <div className="p-5">
          <p className="text-lg font-semibold">{name}</p>
          <p>{email}</p>
          <span className="text-green-800">Exp: {exp}</span>
          <div className="w-[5px] h-[5px] bg-[#808080] mx-1 rounded-full inline-block -translate-y-1"></div>
          <span>Lv. {lvl}</span>
        </div>
      </div>
    </Link>
  );
};

export default PlayerCard;
