import React from "react";
import { Route, Routes } from "react-router-dom";
import DetailPlayer from "./Pages/DetailPlayer";
import FormAddPlayer from "./Pages/FormAddPlayer";
import Player from "./Pages/Player";
import FormUpdatePlayer from "./Pages/FormUpdatePlayer";

const AppRoutes = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Player />} />
        <Route path="/FormAddPlayer" element={<FormAddPlayer />} />
        <Route path="/detail-player" element={<DetailPlayer />} />
        <Route path="/FormUpdatePlayer" element={<FormUpdatePlayer />} />
      </Routes>
    </>
  );
};

export default AppRoutes;
